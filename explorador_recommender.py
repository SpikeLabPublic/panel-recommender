import pandas as pd
#import implicit
import numpy as np
import streamlit as st
import pickle


st.title('Explorador de un recommender')

##############################################################################
########### CARGAMOS DATOS Y DEFINIMOS DICCIONARIOS  #########################
##############################################################################
#Cargamos los datos
@st.cache
def load_data():
    with open('exportar_modelo.pickle', 'rb') as f:
        model_import = pickle.load(f)
    return model_import

# Cargamos datos y definimos los diccionarios de usuarios y productos
model_import = load_data()
data = model_import['data']
users = list(data['users'].unique())
items = list(data['items'].unique())
dict_users = {k: data.query('users_id==@k').iloc[0]['users'] for k in list(data.users_id.unique())} # user_id: user
dict_items = {k: data.query('items_id==@k').iloc[0]['items'] for k in list(data.items_id.unique())} #item_id: item
dict_users_rev = {data.query('users_id==@k').iloc[0]['users']: k   for k in list(data.users_id.unique())} #user: user_id
dict_items_rev = {data.query('items_id==@k').iloc[0]['items']: k  for k in list(data.items_id.unique())} #item: item_id

item_factors = model_import['item_factors']
user_factors = model_import['user_factors']
##############################################################################
########### DEFINIMOS CADA FUNCIONALIDAD DENTRO DE UNA FUNCIÓN  ##############
##############################################################################
def mirar_datos_crudos():
    st.subheader('Aquí se muestran los datos de entrenamiento del modelo y es posible explorarlos a nivel de usuario o producto')
    st.subheader('Datos entrenamiento')
    data = model_import['data']
    st.write(data)
    st.write('Tenemos', data['users'].nunique(),'  usuarios y ',data['items'].nunique(),' items ')

    exploracion_base = st.selectbox("Qué miramos", ['usuarios','items', 'usuario particular', 'item particular'])
    if exploracion_base == 'usuarios':
        st.write('Tenemos los usuarios:', data['users'].unique())
    elif exploracion_base == 'items':
        st.write('Tenemos los items ', data['items'].unique())
    elif exploracion_base == 'usuario particular':
        user_particular = st.selectbox('Lista usuarios', users)
        tabli = data.query('users==@user_particular')
        st.write(tabli)
    elif exploracion_base == 'item particular':
        item_particular = st.selectbox('Lista items', items)
        tabli = data.query('items==@item_particular')
        st.write(tabli)


def similitud_usuario_producto():
    st.subheader('Aquí se muestra la similitud entre el usuario escogido y todos los productos, ordenados desde el más recomendado hasta el menos recomendado')
    user_selected = st.selectbox("Escogemos un usuario", users)
    user = dict_users_rev[user_selected]
    vec_user = user_factors[user]
    similitud = np.matmul(item_factors,vec_user,)
    similitud = similitud.argsort()[::-1] #ahora queda de más a menos
    items_similes = [dict_items[k] for k in similitud]
    st.write('Los items similares del usuario ', user_selected, '(',user, ') son: ', items_similes)
    
    

def recomendar_productos():
    st.subheader('Aquí se muestran las recomendaciones para el usuario escogido, considerando sólo items que no ha consumido')
    user_selected = st.selectbox("Escogemos un usuario", users)
    cajita = st.checkbox('Items nuevos solamente')
    user = dict_users_rev[user_selected]
    vec_user = user_factors[user]
    similitud = np.matmul(item_factors,vec_user,)
    
    similitud = similitud.argsort()[::-1] #ahora queda de más a menos
    #Borramos aquellos items que ya consumió
    items_consumidos = list(data.query('users_id==@user').items_id.unique())
    if cajita:
    	similitud = [j for j in similitud if j not in items_consumidos]
    items_similes = [dict_items[k] for k in similitud]
    st.write('Los items similares del usuario ', user_selected, '(',user, ') son: ', items_similes)


def productos_similares():
    st.subheader('Aquí se muestran los items similares al producto escogido, ordenadas desde el más símil al menos símil.')
    item_selected = st.selectbox("Escogemos un item", items)
    item = dict_items_rev[item_selected]
    vec_item = item_factors[item]
    similitud = np.matmul(item_factors,vec_item,)
    
    similitud = similitud.argsort()[::-1] #ahora queda de más a menos
    #Borramos aquellos items que ya consumió
    items_similes = [dict_items[k] for k in similitud]
    st.write('Los items similares al item ', item_selected, '(',item, ') son: ', items_similes)






##############################################
########### EMPEZAMOS LA APP  ################
##############################################
app_mode = st.sidebar.selectbox("Escoger qué hacer",
         ["Mirar datos crudos", "Recomendar productos a usuario", "Encontrar productos similares"])
if app_mode == "Mirar datos crudos":
    mirar_datos_crudos()
elif app_mode == "Recomendar productos a usuario":
    recomendar_productos()
elif app_mode == "Encontrar productos similares":
    productos_similares()

        


#st.write('Tenemos los usuarios:', str([1,2,3]))


# 
#     if app_mode == "Show instructions":
#         st.sidebar.success('To continue select "Run the app".')
#     elif app_mode == "Show the source code":
#         readme_text.empty()
#         st.code(get_file_content_as_string("app.py"))
#     elif app_mode == "Run the app":
#         readme_text.empty()
#         run_the_app()


# user_items = item_user_data.T.tocsr()

# recommendations = model.recommend(0, user_items)
# recommendations


# similitud = model.recommend(0, user_items, filter_already_liked_items=False)
# similitud

# # find related items
# related = model.similar_items(0,N=3)
# related


from PIL import Image
image = Image.open('logo_spike.png')

st.sidebar.image(image,caption="Powered by Spike", use_column_width=True)